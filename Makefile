clean: clean_pyc
clean_pyc:
	find . -type f -name "*.pyc" -delete || true

run:
	python manage.py runserver

shell:
	python manage.py shell
