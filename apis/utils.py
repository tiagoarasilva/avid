import json

from django import http
from django.utils.functional import LazyObject


class MoreCapableJsonEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, LazyObject):
            o._setup()
            return o._wrapped
        else:
            return json.JSONEncoder.default(self, o)


def JsonResponse(data, status_code=200, cache_age=None, json_encoder_instance=None):
    "A factory function that encodes the data into a HttpResponse."

    if json_encoder_instance:
        response_data = json_encoder_instance.encode(data)
    else:
        response_data = json.dumps(data, cls=MoreCapableJsonEncoder)

    response = http.HttpResponse(response_data, content_type="application/json")
    response.status_code = status_code
    response['Access-Control-Allow-Origin'] = "*"
    response['Access-Control-Allow-Headers'] = "Authorization"
    if cache_age:
        response['Cache-Control'] = "max-age=%i" % cache_age
    return response