# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import RetrieveModelMixin
from apis.utils import JsonResponse

import apis.serializers
import addresses.models


class BaseDjangoRestFrameworkRequest(object):

    def get_django_request(self):
        return self.request._request


class AddressBookViewList(RetrieveModelMixin, GenericViewSet):
    serializer_class = apis.serializers.AddressBookSerializer

    def get_serializer_context(self):
        context = super(AddressBookViewList, self).get_serializer_context()
        return context

    def get_object(self):
        address_book_id = self.kwargs.get('address_book_id')
        try:
            return addresses.models.AddressBook.objects.get(id=address_book_id)
        except addresses.models.AddressBook.DoesNotExist:
            return JsonResponse({'error': 'Not found'})
