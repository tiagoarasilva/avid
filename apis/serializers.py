from rest_framework import serializers


class AddressBookSerializer(serializers.Serializer):
    name = serializers.CharField(source='profile.first_name')
    surname = serializers.CharField(source='profile.last_name')
    mobile_number = serializers.CharField(source='profile.mobile_number')
    email = serializers.CharField(source='profile.email')

    def __init__(self, *args, **kwargs):
        super(AddressBookSerializer, self).__init__(*args, **kwargs)
        # self.fields['addresses'] = self.instance.get_addresses()

