from django.conf.urls import url
from apis.views import AddressBookViewList

urlpatterns = [
    url(r'^address-book/list-(?P<address_book_id>[a-zA-Z0-9-_]+)/$',
        AddressBookViewList.as_view({'get': 'retrieve'}), name='address-book'),
    # url(r'^address-book/list/$', list_address_book, name='address-book'),
]
