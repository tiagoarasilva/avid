# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import AbstractUser
from django.db import models


class Profile(AbstractUser):
    mobile_number = models.CharField(max_length=255, null=True)

    def __unicode__(self):
        return u'{} {}'.format(self.first_name, self.last_name)


class Address(models.Model):
    first_name = models.CharField(max_length=255, null=True)
    last_name = models.CharField(max_length=255, null=True)
    company = models.CharField(max_length=255)
    street_address = models.CharField(max_length=255)
    extended_address = models.CharField(max_length=255)
    locality = models.CharField(max_length=255)
    region = models.CharField(max_length=255)
    postal_code = models.CharField(max_length=16)
    country = models.CharField(max_length=2)
    phone = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=255, blank=True)
    address_book = models.ForeignKey('addresses.AddressBook', null=True, blank=False, related_name='addresses')

    def __unicode__(self):
        return u'%s - %s, %s - %s' % (self.first_name, self.street_address, self.postal_code, self.country)


class AddressBook(models.Model):
    profile = models.ForeignKey(Profile, null=True, blank=True, related_name='address_books')

    def __unicode__(self):
        return u'{name} - {surname}'.format(name=self.profile.first_name,
                                            surname=self.profile.last_name)

    def get_addresses(self):
        return self.addresses.all()
